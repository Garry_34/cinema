const dafaultData = {
  tiketData:null
};
export const state = () => (dafaultData);
export const actions = {
  setTiketData({commit}, data) {
    commit('SET_TIKET_DATA', data);
  },
}
export const mutations = {
  SET_TIKET_DATA(state, data) {
    state.tiketData = data;
  },
}
export const getters = {
  tiketData: state => {
    return state.tiketData;
  }
};
