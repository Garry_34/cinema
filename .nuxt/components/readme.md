# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<BookTiket>` | `<book-tiket>` (components/BookTiket.vue)
- `<Cinema>` | `<cinema>` (components/Cinema.vue)
- `<Header>` | `<header>` (components/Header.vue)
- `<SessionItem>` | `<session-item>` (components/SessionItem.vue)
- `<SmallMovieCard>` | `<small-movie-card>` (components/SmallMovieCard.vue)
