import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _61a40191 = () => interopDefault(import('..\\pages\\search\\index.vue' /* webpackChunkName: "pages/search/index" */))
const _403af888 = () => interopDefault(import('..\\pages\\sessions\\index.vue' /* webpackChunkName: "pages/sessions/index" */))
const _5ba5a16f = () => interopDefault(import('..\\pages\\movie\\_id\\index.vue' /* webpackChunkName: "pages/movie/_id/index" */))
const _ec428244 = () => interopDefault(import('..\\pages\\sessions\\_id\\index.vue' /* webpackChunkName: "pages/sessions/_id/index" */))
const _64e04a8e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/search",
    component: _61a40191,
    name: "search"
  }, {
    path: "/sessions",
    component: _403af888,
    name: "sessions"
  }, {
    path: "/movie/:id",
    component: _5ba5a16f,
    name: "movie-id"
  }, {
    path: "/sessions/:id",
    component: _ec428244,
    name: "sessions-id"
  }, {
    path: "/",
    component: _64e04a8e,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
